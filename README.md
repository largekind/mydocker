# Larksiper's docker

ただの自分用Docker

## Install

docker環境下にて利用 docker自体は適当に頑張る

```bash
docker build .
```

## Usage

```bash
docker run -v mountfolder repository /bin/bash
```

__mountfolder /c/Users/Username/*でしかほぼ不可能__なので注意__
[それ以外のディレクトリをマウントする場合はサイトを参照](https://qiita.com/dojineko/items/f623894ef2436bef890e)  

~~実際に行う場合、自環境に合わせたdocst.shがあるのでそれを利用。~~  
docker-composeを構成できたのでそれを利用。ただしマウントが出来なかった。何故...
やはり無難にdocst.shを利用する

```bash
docker-compose build
```